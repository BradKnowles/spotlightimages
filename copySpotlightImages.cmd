@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET me=%~nx0
SET parent=%~dp0

SET _source="%LOCALAPPDATA%\Packages\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\LocalState\Assets"
SET _dest="%parent%ztemp\raw"
SET _imgDest="%parent%ztemp\images"
SET _logFile="%parent%log.txt"

REM ROBOCOPY %_source% %_dest% /NJH /NJS /NDL /NP /LOG:%_logFile% /TEE
ROBOCOPY %_source% %_dest% /NJH /NJS /NDL /NP /TEE

COPY %_dest% %_imgDest%\*.jpg
